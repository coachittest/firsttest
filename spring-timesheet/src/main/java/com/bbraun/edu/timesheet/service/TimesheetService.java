/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 19.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.service;

import java.util.List;

import com.bbraun.edu.timesheet.domain.Employee;
import com.bbraun.edu.timesheet.domain.Manager;
import com.bbraun.edu.timesheet.domain.Task;

public interface TimesheetService {

	/**
	* @return Finds the busiest task (with the most of employees).
	* Returns {@code null} when tasks are empty.
	*/
	Task busiestTask();

	/**
	* Finds all the tasks for the employee.
	* @param e Employee
	* @return Tasks
	*/
	List<Task> tasksForEmployee(Employee e);

	/**
	* Finds all the tasks for the manager.
	* @param m Manager
	* @return Tasks
	*/
	List<Task> tasksForManager(Manager m);
}
