/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 30.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.service.dao;

import com.bbraun.edu.timesheet.domain.Manager;
import com.bbraun.edu.timesheet.service.GenericDao;

/**
 * DAO of Manager.
 */
public interface ManagerDao extends GenericDao<Manager, Long> {
    /**
     * Tries to remove manager from the system.
     * @param manager Manager to remove
     * @return {@code true} if manager is not assigned to any task.
     * Else {@code false}.
     */
    boolean removeManager(Manager manager);
}
