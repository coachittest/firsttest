/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 09.10.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.web.commands;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.bbraun.edu.timesheet.domain.Timesheet;


public class TimesheetCommand {

    @NotNull
    @Range(min = 1, message = "Hours must be 1 or greater")
    private Integer hours;
    private Timesheet timesheet;

    // default c-tor for bean instantiation
    public TimesheetCommand() {}

    public TimesheetCommand(Timesheet timesheet) {
        hours = timesheet.getHours();
        this.timesheet = timesheet;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Timesheet getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(Timesheet timesheet) {
        this.timesheet = timesheet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TimesheetCommand that = (TimesheetCommand) o;

        if (hours != null ? !hours.equals(that.hours) : that.hours != null) {
            return false;
        }
        if (timesheet != null ? !timesheet.equals(that.timesheet) : that.timesheet != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = hours != null ? hours.hashCode() : 0;
        result = 31 * result + (timesheet != null ? timesheet.hashCode() : 0);
        return result;
    }
}
